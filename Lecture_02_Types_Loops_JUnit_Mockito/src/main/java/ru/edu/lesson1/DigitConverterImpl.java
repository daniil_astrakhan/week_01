package ru.edu.lesson1;

public class DigitConverterImpl implements DigitConverter {

    /**
     * Преобразование целого десятичного числа в другую систему счисления
     *
     * @param digit - число
     * @param radix - основание системы счисления
     * @return
     * @throws IllegalArgumentException если digit отрицательное число
     * @throws IllegalArgumentException если radix меньше единицы
     */
    @Override
    public String convert(int digit, int radix) {
        if (digit == 0) {
            return "0";
        }
        int tmp = digit;
        int result = 0;
        StringBuffer str = new StringBuffer(result);
        while (tmp > 0) {
            str = str.append(tmp % radix);
            tmp /= radix;
        }

        str.reverse();
        String finish = str.toString();
        return finish;
    }
}
