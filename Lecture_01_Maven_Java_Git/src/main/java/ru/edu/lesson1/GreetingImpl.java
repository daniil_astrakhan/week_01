package ru.edu.lesson1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GreetingImpl implements IGreeting {

    private List<Hobby> hobbies = new ArrayList<>();
    private String firstName = "Даниил";
    private String secondName = "Александрович";
    private String lastName = "Атяшкин";
    private String bitBucketUrl = "git clone https://Daniil_Astrakhan@bitbucket.org/daniil_astrakhan/week_01.git";
    private String phone = "+79064551386";
    private String courseExpectation = "Получить значния по Java";
    private String educationInfo = "Java Developer: от Hello World до собственного клиент-серверного приложения, группа Б";

    public GreetingImpl() {
        hobbies.add(new Hobby("1", "Рыбалка", "Люблю рыбалку"));
        hobbies.add(new Hobby("2", "Компьютеры", "Все что связано с компьютерами"));
    }


    /**
     * Get first name.
     */
    @Override
    public String getFirstName() {
        return firstName;
    }

    /**
     * Get second name
     */
    @Override
    public String getSecondName() {
        return secondName;
    }

    /**
     * Get last name.
     */
    @Override
    public String getLastName() {

        return lastName;
    }

    /**
     * Get hobbies.
     */
    @Override
    public Collection<Hobby> getHobbies() {
        return hobbies;
    }

    /**
     * Get bitbucket url to your repo.
     */
    @Override
    public String getBitbucketUrl() {

        return bitBucketUrl;
    }

    /**
     * Get phone number.
     */
    @Override
    public String getPhone() {

        return phone;
    }

    /**
     * Your expectations about course.
     */
    @Override
    public String getCourseExpectation() {

        return courseExpectation;
    }

    /**
     * Print your university and faculty here.
     */
    @Override
    public String getEducationInfo() {

        return educationInfo;
    }

    @Override
    public String toString() {
        return "GreetingImpl{" +
                "hobbies=" + hobbies +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", bitBucketUrl='" + bitBucketUrl + '\'' +
                ", phone='" + phone + '\'' +
                ", courseExpectation='" + courseExpectation + '\'' +
                ", educationInfo='" + educationInfo + '\'' +
                '}';
    }
}
