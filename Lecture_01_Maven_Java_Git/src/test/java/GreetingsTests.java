import org.junit.Assert;
import org.junit.Test;
import ru.edu.lesson1.GreetingImpl;
import ru.edu.lesson1.Hobby;
import ru.edu.lesson1.IGreeting;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

public class GreetingsTests {

    private IGreeting greeting = new GreetingImpl();

    @Test
    public void size_Test() {

        Assert.assertNotNull(greeting);

        assertEquals("Даниил", greeting.getFirstName());
        assertEquals("Александрович", greeting.getSecondName());
        assertEquals("Атяшкин", greeting.getLastName());
        assertEquals("git clone https://Daniil_Astrakhan@bitbucket.org/daniil_astrakhan/week_01.git", greeting.getBitbucketUrl());
        assertEquals("+79064551386", greeting.getPhone());
        assertEquals("Получить значния по Java", greeting.getCourseExpectation());
        assertEquals("Java Developer: от Hello World до собственного клиент-серверного приложения, группа Б", greeting.getEducationInfo());

        // check your methods
    }

}
